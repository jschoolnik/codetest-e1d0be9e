'use strict';

var gulp      = require('gulp'),
    nodemon   = require('gulp-nodemon'),
    bs        = require('browser-sync'),
    reload    = bs.reload,
    when      = require('gulp-if'),
    shell     = require('gulp-shell');


// the paths to app files
var paths = {
  // all client app js files
  scripts: ['app/**/*.js'],
  html: ['app/**/*.html', 'index.html'],
  styles: ['public/css/styles.css'],
  test: ['tests/*.js']
};

// Auto update changes
gulp.task('start', ['serve'],function () {
  bs({
    notify: true,
    // address for server,
    injectChanges: true,
    files: paths.scripts.concat(paths.html, paths.styles),
    proxy: 'localhost:8000'
  });
});

// start node server using nodemon
gulp.task('serve', function() {
  nodemon({script: './server.js', ignore: 'node_modules/**/*.js'});
});

gulp.task('default', ['start']);
