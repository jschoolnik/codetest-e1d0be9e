 // app/routes.js

// grab the list model we just created
var List = require('./models/list');

    module.exports = function(app) {

        // server routes ===========================================================

        // sample api route
        app.get('/api/listItems', function(req, res) {
            List.find(function(err, listItems) {
                if (err)
                    res.send(err);
                res.json(listItems);
            });
        });


        app.post('/api/listItems', function(req, res) {
            var userEntry = new List({
                description: req.body.description,
                done: req.body.done,
                priority: req.body.priority
            });
            userEntry.save(function(err, resp) {
                if (err) {
                    res.send(err);
                } else {
                   res.send({message:'the list item has been saved'}); 
                }
            });
        });


        app.delete('/api/listItems/:listItem_id', function(req, res) {
            var query = { _id: req.params.listItem_id };
            return List.remove(query, function(err) {
                if (err) {
                    res.status(204).send(err);
                } else {
                    res.status(201).send(query);
                }
            });
        });


        app.put('/api/listItems/', function(req, res) {
            var query = { _id: req.body._id };
            var update = {
                description: req.body.description,
                done: req.body.done,
                priority: req.body.priority                
            };
            return List.update(query, update, function(err, success) {
                if (err) {
                    res.status(204).send(err);
                } else {
                    res.status(201).send(success);
                }
            });
        });


        // frontend routes =========================================================
        // route to handle all angular requests
        app.get('*', function(req, res) {
            res.sendfile('./public/index.html');
        });

    };
