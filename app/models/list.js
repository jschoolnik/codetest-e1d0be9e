// app/models/list.js
var mongoose = require('mongoose');

module.exports = mongoose.model('List', {
  description : {type : String, default: ''},
  done : {type : Boolean, default: false},
  priority : {type : Number, default: 0}
});
