describe('MainController', function () {
  var $scope, $rootScope, $location, createController, $httpBackend, ListService;

  // using angular mocks, inject the injector
  // to retrieve dependencies
  beforeEach(module('sampleApp'));
  beforeEach(inject(function($injector) {

    // mock out dependencies
    $rootScope = $injector.get('$rootScope');
    $httpBackend = $injector.get('$httpBackend');
    ListService = $injector.get('ListService');
    $location = $injector.get('$location');

    $scope = $rootScope.$new();

    var $controller = $injector.get('$controller');

    createController = function () {
      return $controller('MainController', {
        $scope: $scope,
        ListService: ListService,
        $location: $location
      });
    };

    createController();
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should have a list property on the $scope', function() {
    expect($scope.todoList).to.be.an('array');
  });

  it('should have a get all items method on the $scope', function () {
    expect($scope.getAllToDos).to.be.a('function');
  });

  it('should have a add item method on the $scope', function () {
    expect($scope.addListItem).to.be.a('function');
  });

});
