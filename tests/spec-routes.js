describe('Routing', function () {
  var $route;
  beforeEach(module('sampleApp'));

  beforeEach(inject(function($injector){
    $route = $injector.get('$route');
  }));

  it('Should have / route, template, and controller', function () {
    expect($route.routes['/']).to.be.ok();
    expect($route.routes['/'].controller).to.be('MainController');
    expect($route.routes['/'].templateUrl).to.be('views/home.html');
  });

  it('Should have /done route, template, and controller', function () {
    expect($route.routes['/done']).to.be.ok();
    expect($route.routes['/done'].controller).to.be('DoneController');
    expect($route.routes['/done'].templateUrl).to.be('views/done.html');
  });

});