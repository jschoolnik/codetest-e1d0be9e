// public/js/services/DoneService.js
angular.module('ListService', []).factory('ToDoService', ['$http', function($http) {

  return {
    // call to get all listItems
    get : function() {
      return $http.get('/api/listItems');
    },

    // call to POST and create a new listItem
    create : function(listItem) {
      return $http.post('/api/listItems', listItem);
    },

    // call to DELETE a listItem
    delete : function(id) {
      return $http.delete('/api/listItems/' + id);
    },

    // call to UPDATE a listItem
    put : function(id) {
      return $http.put('/api/listItems/', id);
    }
  };    

}]);