// public/js/appRoutes.js
    angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })

        // Completed list items page that will use the DoneController
        .when('/done', {
            templateUrl: 'views/done.html',
            controller: 'DoneController'
        });

    $locationProvider.html5Mode(true);

}]);