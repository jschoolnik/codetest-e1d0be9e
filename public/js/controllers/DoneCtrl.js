// public/js/controllers/DoneCtrl.js
angular.module('DoneCtrl', []).controller('DoneController', function($scope, ToDoService) {

  $scope.completedTodoList = [];

  // --- GET -----------------------------

  //get all list items and filter by complete
  var getAllCompletedToDos = function () {
    ToDoService.get(function() {
    }).then(function(allListItems) {
      for (var i = 0; i < allListItems.data.length; i++) {
        if (allListItems.data[i].done === true) {
          $scope.completedTodoList.push(allListItems.data[i]);
        }
      }
      //sort by priority
      if ($scope.completedTodoList.length > 1) {  
        $scope.completedTodoList.sort(function(a, b) {
          return a.priority - b.priority;
        });
      }

    });
  };
  // Get all list items in DB
  getAllCompletedToDos();

  // --- DELETE --------------------------

  $scope.delete = function(item) {
    ToDoService.delete(item._id)
      .catch(function(err) {
        console.log('Error deleting item: ', err);
      });
    $scope.completedTodoList.splice($scope.completedTodoList.indexOf(item), 1);
  };


});
