// public/js/controllers/MainCtrl.js
angular.module('MainCtrl', []).controller('MainController', function($scope, ToDoService) {

  $scope.todoList = [];
  $scope.priority = 0;
  $scope.currentItem = {};

  // --- GET -----------------------------

  var getAllToDos = function () {
    $scope.todoList = [];
    ToDoService.get(function() {

    }).then(function(allListItems) {
      for (var i = 0; i < allListItems.data.length; i++) {
        $scope.todoList.push(allListItems.data[i]);
      }
      resortPriority();
    });
  };
  // Get all list items in DB
  getAllToDos();


  // --- CREATE --------------------------

  var addToDOtoDB = function () {
    ToDoService.create($scope.currentItem)
      .catch(function (err) {
        console.log('Add ToDo item API from client not working: ', err);
      });
  };

  $scope.addListItem = function() {
    updatePriority();
    $scope.currentItem = {
      'description': $scope.itemDescription,
      'done': false,
      'priority': $scope.itemPriority || $scope.priority + 1
    };
    addToDOtoDB();
    getAllToDos();
    resortPriority();
    $scope.itemDescription = '';
    $scope.itemPriority = undefined;
  };


  // --- UPDATE --------------------------

  $scope.updateListItem = function(item) {
    ToDoService.put(item)
      .catch(function(err) {
        console.log('Error updating item: ', err);
      }).then(getAllToDos());
  };


  // --- DELETE --------------------------

  $scope.delete = function(item) {
    ToDoService.delete(item._id)
      .catch(function(err) {
        console.log('Error deleting item: ', err);
      });
    $scope.todoList.splice($scope.todoList.indexOf(item), 1);
  };
    

  // --- Helper Functions ----------------
  function resortPriority() {
    $scope.todoList.sort(function(a, b) {
      return a.priority - b.priority;
    });
  };

  function updatePriority() {
    for(var i = 0; i < $scope.todoList.length; i++) {
      if ($scope.todoList[i].priority > $scope.priority) {
        $scope.priority = $scope.todoList[i].priority;
      }
    }
  };


});